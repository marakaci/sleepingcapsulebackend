from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from accounts.models import User, Person


@admin.register(User)
class MyUserAdmin(UserAdmin):
    list_display = ('username', 'is_staff')
    filter_horizontal = []
    list_filter = []


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    pass
