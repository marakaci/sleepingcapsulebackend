from rest_framework.permissions import BasePermission


class IsClient(BasePermission):
    """
    Allow only clients.
    This isn't strictly required, since you could use an empty
    permission_classes list, but it's useful because it makes the intention
    more explicit.
    """

    def has_permission(self, request, view):
        return request.user.is_person and request.user.person.is_client


class IsPerson(BasePermission):
    """
    Allow only people.
    This isn't strictly required, since you could use an empty
    permission_classes list, but it's useful because it makes the intention
    more explicit.
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.is_person


class IsCapsule(BasePermission):
    """
    Allow only capsules.
    This isn't strictly required, since you could use an empty
    permission_classes list, but it's useful because it makes the intention
    more explicit.
    """

    def has_permission(self, request, view):
        return request.user.is_capsule
