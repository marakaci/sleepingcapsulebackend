# Create your views here.
from django.db.transaction import atomic
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError
from rest_framework.generics import RetrieveAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.views import ObtainJSONWebToken as BaseObtainJSONWebToken

from accounts.models import User, Person
from accounts.permissions import IsPerson
from accounts.serializers import PersonSerializer, UserSerializer, PersonSelfSerializer
from capsules.serializers import HotelSerializer


class ObtainJSONWebToken(BaseObtainJSONWebToken):
    def post(self, request, *args, **kwargs):
        """
        Authenticates request and appends user type to it
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        response = super(ObtainJSONWebToken, self).post(request, *args, **kwargs)
        if response.status_code == 200:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                user = serializer.object.get('user') or request.user
                if user.is_capsule:
                    response.data['is_capsule'] = True
                    response.data['capsule_pk'] = user.capsule.pk
                elif user.is_person:
                    response.data['is_person'] = True
                    response.data['person_pk'] = user.person.pk
                    if user.person.is_hotel_owner:
                        response.data['is_hotel_owner'] = True
                        response.data['hotel'] = HotelSerializer(user.person.hotel).data
                else:
                    raise ParseError("Wrong user type")
        return response


class SelfUserDetailView(RetrieveAPIView):
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.request:
            user = self.request.user
            if user.is_person:
                return PersonSelfSerializer
            return UserSerializer

    def get_object(self):
        self.kwargs['pk'] = self.request.user.pk
        obj = super(SelfUserDetailView, self).get_object()
        if obj.is_person:
            return obj.person
        return obj

    def retrieve(self, request, *args, **kwargs):
        """
        Retrieves user
        :param request:
        :param args:
        :param kwargs:
        :return: user detail
        """
        return super(SelfUserDetailView, self).retrieve(request, *args, **kwargs)


class UserViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        """
        creates a user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(UserViewSet, self).create(request, *args, **kwargs)


class PersonViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    permission_classes = [AllowAny]

    @action(detail=False, url_path='hotel-owner', methods=['POST'])
    @atomic
    def create_hotel_owner(self, request, *args, **kwargs):
        try:
            hotel_name = request.data.pop('hotel_name')
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'hotel_name': "This field is required"})
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        person_pk = serializer.data['id']
        hotel_serializer = HotelSerializer(data={'name': hotel_name, 'person': person_pk})
        hotel_serializer.is_valid(raise_exception=True)
        hotel_serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def create(self, request, *args, **kwargs):
        """
        creates a person
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(PersonViewSet, self).create(request, *args, **kwargs)

    @action(detail=False, url_path='fingerprint', methods=['POST'], permission_classes=[IsPerson])
    def save_person_fingerprint(self, request, *args, **kwargs):
        try:
            fingerprint = request.data.pop('fingerprint')
            print("fingerprint", fingerprint)
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'fingerprint': "This field is required"})
        person = request.user.person
        person.fingerprint = fingerprint
        person.save()
        return Response()
