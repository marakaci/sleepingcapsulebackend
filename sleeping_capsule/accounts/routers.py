from rest_framework import routers

from .views import UserViewSet, PersonViewSet

router = routers.DefaultRouter()
router.register('user', UserViewSet, base_name='user')
router.register('person', PersonViewSet, base_name='person')
