from django.db.models import Sum, Count
from django.utils.datetime_safe import date
from rest_framework import serializers

from accounts.models import User, Person
from capsules.models import Booking
from utils.serializer_mixins import QueryFieldsDepthModelSerializer


class UserSerializer(QueryFieldsDepthModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_capsule', 'is_person', 'password', 'last_login')
        extra_kwargs = {
            'password': {'write_only': True},
            'is_capsule': {'read_only': True},
            'is_person': {'read_only': True},
        }

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user


class PersonSerializer(QueryFieldsDepthModelSerializer):
    user = UserSerializer(read_only=True)
    username = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = Person
        fields = ('id', 'user', 'username', 'password', 'first_name', 'last_name', 'full_name', 'email', 'avatar', 'is_hotel_owner', 'hotel')
        extra_kwargs = {
            'hotel': {'read_only': True},
        }

    def create(self, validated_data):
        username = validated_data.pop('username')
        password = validated_data.pop('password')
        user_serializer = UserSerializer(data={"username": username, "password": password})
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()
        person = Person.objects.create(user=user, **validated_data)
        return person


class PersonSelfSerializer(PersonSerializer):
    def to_representation(self, instance):
        res = super(PersonSelfSerializer, self).to_representation(instance)
        res['amount_of_money_spent'] = Booking.objects.filter(person=instance). \
                                           aggregate(total=Sum('capsule__price'))['total'] or 0
        res['amount_of_visited_capsules'] = Booking.objects.filter(person=instance). \
                                                aggregate(total=Count('capsule_id', distinct=True))['total'] or 0
        from capsules.serializers import BookingSerializer
        next_booking = Booking.objects.filter(person=instance, date__gte=date.today()).order_by('date').first()
        res['next_booking'] = BookingSerializer(next_booking).data if next_booking else None
        return res


class PersonCreateSerializer(PersonSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)

    class Meta(PersonSerializer.Meta):
        pass
