from django.urls import path, include
from rest_framework_jwt.views import refresh_jwt_token

from accounts.views import SelfUserDetailView, ObtainJSONWebToken
from .routers import router

urlpatterns = [
    path('login/', ObtainJSONWebToken.as_view()),
    path('token-refresh/', refresh_jwt_token),
    path('user/self/', SelfUserDetailView.as_view()),
    path('', include(router.urls)),

]
