from datetime import timedelta

from django.contrib.auth.tokens import default_token_generator
from django.db.models import Sum, Count, FloatField
from django.http import Http404
from django.utils.crypto import get_random_string
from django.utils.datetime_safe import datetime
from django.utils.timezone import now
from django.utils.translation import ugettext as _
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler

from accounts.models import User
from accounts.permissions import IsCapsule
from capsules.filters import CapsuleFilter
from capsules.models import Capsule, Preferences, Booking, Payment, Hotel
from capsules.permissions import IsCapsuleOrHotelOwnerOrReadOnly
from capsules.serializers import CapsuleSerializer, PreferencesSerializer, PaymentSerializer, HotelSerializer, \
    BookingCreateSerializer, CapsuleCreateSerializer
from utils.heplers import get_object_or_None


class HostelViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = HotelSerializer
    queryset = Hotel.objects.all()

    def list(self, request, *args, **kwargs):
        """
        list of hostels
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(HostelViewSet, self).list(request, *args, **kwargs)

    @staticmethod
    def _get_money_and_bookings_for_gap(hotel: Hotel, initial_date_timestamp, gap_range, timedelta_key):
        gap_money = []
        gap_booking = []
        bookings = Booking.objects.filter(capsule__hotel=hotel)
        for i in range(gap_range):
            timestamp = initial_date_timestamp - timedelta(**{timedelta_key: i})
            try:
                booking = (bookings
                           .filter(date__gt=(timestamp - timedelta(**{timedelta_key: 1})).date(),
                                   date__lte=(timestamp).date())
                           .aggregate(sum=Sum('capsule__price', output_field=FloatField()),
                                      bookings=Count('id')))
                gap_money.append({'date': timestamp, 'value': booking['sum']})
                gap_booking.append({'date': timestamp, 'value': booking['bookings']})
            except Booking.DoesNotExist:
                pass
        return {'money': reversed(gap_money), 'bookings': reversed(gap_booking)}

    @action(detail=True, methods=['GET'], url_path='statistics')
    def statistics(self, request, *args, **kwargs):
        """
        Makes statistics for the last hour, day and month
        :param request:
        :param args:
        :param kwargs:
        :return: last week, month and half-year bookings and sum data
        """
        hotel = self.get_object()
        today = datetime.today()
        last_week_indicators = self._get_money_and_bookings_for_gap(hotel, today, 7, 'days')
        last_month_indicators = self._get_money_and_bookings_for_gap(hotel, today, 31, 'days')
        last_half_year_indicators = self._get_money_and_bookings_for_gap(hotel, today, 24, 'weeks')
        data = {
            'last_week': last_week_indicators,
            'last_month': last_month_indicators,
            'last_half_year': last_half_year_indicators
        }
        return Response(data=data)


class CapsuleViewSet(ModelViewSet):
    permission_classes = [IsCapsuleOrHotelOwnerOrReadOnly]
    serializer_class = CapsuleSerializer
    queryset = Capsule.objects.all()
    filterset_class = CapsuleFilter

    def get_serializer_class(self):
        if not self.request or self.request.method in SAFE_METHODS:
            return CapsuleSerializer
        return CapsuleCreateSerializer

    def check_object_permissions(self, request, obj):
        if request.method == 'DELETE' and obj.has_future_bookings:
            self.permission_denied(request, message=_("Capsule has future bookings"))
        super(CapsuleViewSet, self).check_object_permissions(request, obj)

    def perform_create(self, serializer):
        if self.request.user.is_capsule:
            user = self.request.user
            serializer.save(user=user)
        elif self.request.user.is_person and self.request.user.person.is_hotel_owner:
            user = User(username=get_random_string(length=255))
            user.set_password(get_random_string(length=255))
            user.save()
            serializer.save(user=user, hotel=self.request.user.person.hotel)
        else:
            raise RuntimeError("Wrong user type")

    def create(self, request, *args, **kwargs):
        """
        creates a capsule
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(CapsuleViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        """
        updates a capsule
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(CapsuleViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """
        destroys a capsule
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(CapsuleViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        """
        retrieve a capsule
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(CapsuleViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        """
        list of capsules
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(CapsuleViewSet, self).list(request, *args, **kwargs)

    @action(detail=True, url_path='who-can-login-now', methods=['GET'], permission_classes=[IsCapsule])
    def who_can_login_now(self, *args, **kwargs):
        capsule = self.request.user.capsule
        today_date = now().today()
        booking = get_object_or_None(Booking, capsule=capsule, date=today_date)
        if booking:
            person = booking.person
            token = jwt_encode_handler(jwt_payload_handler(person.user))
            data = {'pk': person.pk, 'fingerprint': person.fingerprint, 'token': token}
            return Response(data=data)
        return Response(data={})


class PreferencesViewSet(mixins.CreateModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.DestroyModelMixin,
                         GenericViewSet):
    serializer_class = PreferencesSerializer
    queryset = Preferences.objects.all()

    def perform_update(self, serializer):
        serializer.save(person=self.request.user.person)

    def perform_create(self, serializer):
        serializer.save(person=self.request.user.person)

    def check_object_permissions(self, request, obj):
        super(PreferencesViewSet, self).check_object_permissions(request, obj)
        if not request.user.is_person or request.user.person != obj.person:
            self.permission_denied(
                request, message=_("Only the person can update his preferences")
            )

    def get_object(self):
        preference, _ = Preferences.objects.get_or_create(person=self.request.user.person)
        return preference

    def create(self, request, *args, **kwargs):
        """
        creates a person's preferences
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(PreferencesViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        """
        updates a person's preferences
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(PreferencesViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """
        destroys a person's preferences
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(PreferencesViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        """
        retrieve a person's preferences
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return super(PreferencesViewSet, self).retrieve(request, *args, **kwargs)


class BookingViewSet(mixins.CreateModelMixin,
                     mixins.DestroyModelMixin,
                     GenericViewSet):
    serializer_class = BookingCreateSerializer
    queryset = Booking.objects.all()

    def check_permissions(self, request):
        super(BookingViewSet, self).check_permissions(request)
        if not request.user.is_person:
            self.permission_denied(request, message=_("Only people have permissions to the endpoint"))

    @action(detail=False, methods=['GET'], url_name='get', url_path='capsule/(?P<capsule_pk>\d+)')
    def get_capsule_bookings_by_capsule(self, request, *args, **kwargs):
        """
        list of bookings for the capsule
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        capsule = get_object_or_404(Capsule, pk=self.kwargs.pop('capsule_pk'))
        queryset = capsule.booking_set.all()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        creates a booking
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        return super(BookingViewSet, self).create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(person=self.request.user.person)

    def destroy(self, request, *args, **kwargs):
        """
        deletes a booking
        :param request: 
        :param args: 
        :param kwargs: 
        :return: 
        """
        return super(BookingViewSet, self).destroy(request, *args, **kwargs)


class PaymentViewSet(mixins.CreateModelMixin, GenericViewSet):
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()

    def check_permissions(self, request):
        super(PaymentViewSet, self).check_permissions(request)
        if not request.user.is_person:
            self.permission_denied(request, message=_("Only people have permissions to the endpoint"))

    @action(detail=False, methods=['POST'], url_path='validate')
    def validate_payment(self, request, *args, **kwargs):
        """
        Makes payment token for user
        :param request:
        :param args:
        :param kwargs:
        :return: token
        """
        token = default_token_generator.make_token(request.user)
        return Response(data={'token': token})

    @action(detail=False, methods=['POST'], url_path='pay')
    def pay(self, request, *args, **kwargs):
        """
        Checks token and confirms payment
        :param request:
        :param args:
        :param kwargs:
        :return: status code
        """
        if not default_token_generator.check_token(request.user, request.data.get('token')):
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'token': 'Invalid token'})
        try:
            booking_pk = int(request.data.get('booking_id'))
        except (KeyError, ValueError):
            raise Response(status=status.HTTP_400_BAD_REQUEST, data={'booking_id': 'Field is required'})
        booking = get_object_or_404(Booking, pk=booking_pk)
        try:
            payment = booking.payment
        except Payment.DoesNotExist:
            raise Http404
        payment.is_paid = True
        payment.save()
        return Response(status=201)
