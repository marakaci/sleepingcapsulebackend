from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request

from capsules.models import Capsule


class IsCapsuleOrHotelOwnerOrReadOnly(BasePermission):
    """
    The request is authenticated as a user, or is a read-only request.
    """

    def has_permission(self, request: Request, view):
        return (
                request.method in SAFE_METHODS or
                (request.user.is_authenticated and request.user.is_capsule) or
                (request.user.is_authenticated and request.user.is_person and request.user.person.is_hotel_owner)

        )

    def has_object_permission(self, request, view, obj: Capsule):
        return (
                request.method in SAFE_METHODS or
                (request.user.is_authenticated and request.user.is_capsule and obj == request.user.capsule) or
                (request.user.is_authenticated and request.user.is_person and
                 request.user.person.is_hotel_owner and obj.hotel == request.user.person.hotel)
        )
