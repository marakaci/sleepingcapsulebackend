from datetime import timedelta

from django.db.transaction import atomic
from django.utils.datetime_safe import date
from rest_framework import serializers

from accounts.models import Person
from accounts.serializers import UserSerializer
from capsules.models import Capsule, Preferences, Booking, Payment, Hotel
from utils.serializer_mixins import QueryFieldsDepthModelSerializer


class CapsuleSerializer(QueryFieldsDepthModelSerializer):
    user = UserSerializer()

    availability = serializers.SerializerMethodField()

    class Meta:
        model = Capsule
        fields = ['id', 'user', 'hotel', 'name', 'description', 'price', 'photo', 'lat', 'lng', 'release_time',
                  'availability', 'has_future_bookings']
        depth = 1

    def get_availability(self, obj):
        start_date = getattr(obj, 'selected_date', date.today())
        availability = []
        for i in range(7):
            availability.append({'date': start_date.strftime("%Y-%m-%d"),
                                 'available': not obj.booking_set.filter(date=start_date).exists(),
                                 'short_date': start_date.strftime("%d")})
            start_date += timedelta(days=1)
        return availability


class CapsuleCreateSerializer(CapsuleSerializer):
    user = UserSerializer(required=False, allow_null=True)


class HotelSerializer(QueryFieldsDepthModelSerializer):
    capsule_set = CapsuleSerializer(many=True, read_only=True)

    class Meta:
        model = Hotel
        fields = ['id', 'name', 'person', 'capsule_set']
        depth = 0


class PreferencesSerializer(QueryFieldsDepthModelSerializer):
    class Meta:
        model = Preferences
        fields = ['id', 'temperature', 'humidity', 'luminosity', 'go_bed_time', 'wake_up_time', 'person']
        depth = 0


class BookingSerializer(QueryFieldsDepthModelSerializer):
    class Meta:
        model = Booking
        fields = ['id', 'person', 'date', 'capsule']
        depth = 1


class BookingCreateSerializer(BookingSerializer):
    person = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), required=False)

    class Meta(BookingSerializer.Meta):
        depth = 0

    @atomic
    def create(self, validated_data):
        booking = super(BookingSerializer, self).create(validated_data)
        Payment.objects.create(booking=booking)
        return booking


class PaymentSerializer(QueryFieldsDepthModelSerializer):
    class Meta:
        model = Payment
        fields = ['id', 'booking', 'is_paid']
        depth = 0
