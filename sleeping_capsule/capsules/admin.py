from django.contrib import admin

from capsules.models import Capsule, Hotel, Booking, Preferences, Payment


@admin.register(Capsule)
class CapsuleAdmin(admin.ModelAdmin):
    pass


@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    pass


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    pass


@admin.register(Preferences)
class PreferencesAdmin(admin.ModelAdmin):
    pass


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    pass
