from rest_framework import routers

from .views import BookingViewSet, CapsuleViewSet, HostelViewSet, PaymentViewSet, PreferencesViewSet

router = routers.DefaultRouter()
router.register('capsule', CapsuleViewSet, base_name='capsule')
router.register('hotel', HostelViewSet, base_name='hotel')
router.register('booking', BookingViewSet, base_name='booking')
router.register('payment', PaymentViewSet, base_name='payment')
router.register('preferences', PreferencesViewSet, base_name='preferences')
