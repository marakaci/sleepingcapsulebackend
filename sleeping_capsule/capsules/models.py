from django.db import models
from django.utils.datetime_safe import date

from accounts.models import Person, User


class Hotel(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        super(Hotel, self).save(*args, **kwargs)
        if getattr(self, 'person', None) and not self.person.is_hotel_owner:
            self.person.is_hotel_owner = True
            self.person.save()


class Capsule(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=512)
    description = models.TextField()
    photo = models.ImageField(upload_to='capsule/photos/', null=True)
    lat = models.DecimalField(max_digits=8, decimal_places=5)
    lng = models.DecimalField(max_digits=8, decimal_places=5)
    release_time = models.TimeField()
    price = models.DecimalField(max_digits=8, decimal_places=2)

    def save(self, *args, **kwargs):
        user = getattr(self, 'user', None)
        if user:
            self.user.is_capsule = True
            self.user.save()

        super(Capsule, self).save(*args, **kwargs)

    @property
    def has_future_bookings(self, *args, **kwargs):
        return Booking.objects.filter(date__gte=date.today(), capsule=self).exists()


class Preferences(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    temperature = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    humidity = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    luminosity = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    go_bed_time = models.TimeField(null=True, blank=True)
    wake_up_time = models.TimeField(null=True, blank=True)


class Booking(models.Model):
    capsule = models.ForeignKey(Capsule, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    date = models.DateField()

    class Meta:
        ordering = ['-date']
        unique_together = ['capsule', 'date']


class Payment(models.Model):
    booking = models.OneToOneField(Booking, on_delete=models.PROTECT)
    is_paid = models.BooleanField(default=False)
