from django.db.models import Value, DateField
from django_filters import rest_framework as filters

from capsules.models import Capsule, Booking


class CapsuleFilter(filters.FilterSet):
    date = filters.DateFilter(method='filter_date')

    class Meta:
        model = Capsule
        fields = ['date', 'hotel']

    def filter_date(self, queryset, name, value):
        return queryset.exclude(id__in=Booking.objects.filter(date=value)) \
            .annotate(selected_date=Value(value, DateField()))
