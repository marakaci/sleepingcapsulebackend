��          �      <      �  )   �  9   �  ,     ;   B  *   ~  0   �  5   �       F   #  *   j     �     �     �  
   �  	   �     �     �  B  �  )   $  9   N  ,   �  ;   �  *   �  0     5   M     �  F   �  *   �                 
   )  	   4     >     K                                        
   	                                                          A user with that username already exists. Designates whether the user can log into this admin site. Device can create measurement only to itself Only basic/premium clients have permissions to the endpoint Only capsules are able to hit the endpoint Only customers  have permissions to the endpoint Only premium clients have permissions to the endpoint Probable pollution Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. There is a suspection of pollution in area active date joined email address first name last name staff status username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 A user with that username already exists. Designates whether the user can log into this admin site. Device can create measurement only to itself Only basic/premium clients have permissions to the endpoint Only capsules are able to hit the endpoint Only customers  have permissions to the endpoint Only premium clients have permissions to the endpoint Probable pollution Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. There is a suspection of pollution in area active date joined email address first name last name staff status username 