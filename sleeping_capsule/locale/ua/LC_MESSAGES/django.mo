��          �            h  )   i  ,   �  ;   �  *   �  0   '  5   X     �  F   �  *   �            
   (  	   3     =    F  C   ^  Z   �  m   �  b   k  J   �  \     %   v     �  7        T  
   e     p     x  
   �                                                         
       	                A user with that username already exists. Device can create measurement only to itself Only basic/premium clients have permissions to the endpoint Only capsules are able to hit the endpoint Only customers  have permissions to the endpoint Only premium clients have permissions to the endpoint Probable pollution Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. There is a suspection of pollution in area active email address first name last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Користувач з таким логіном вже існує Пристрій може створити показники тільки для себе Лише основні / преміальні клієнти мають права на функціонал Тільки присторій може звертатия до цього функціоналу Лише компанії  мають права на функціонал Лише преміальні клієнти мають права на функціонал Можливе забруднення Обов'язкове поле. Не більш ніж 150 символів. Тільки букви, цифри та @/./+/-/_ існує загроза забрудення води активний почта ім'я прізвище Логін 