From python:3.7
ENV PYTHONUNBUFFERED 1

ADD ./sleeping_capsule/requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt


ADD ./sleeping_capsule /srv/sleeping_capsule/capsule
#ADD ./conf /srv/sleeping_capsule/conf

WORKDIR /srv/sleeping_capsule/capsule
RUN mkdir /srv/sleeping_capsule/log

ARG no_static
RUN if [ -z ${no_static+x} ]; then ./manage.py collectstatic --noinput; else echo "skipping static"; fi


#ADD nginx/ /etc/nginx

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

